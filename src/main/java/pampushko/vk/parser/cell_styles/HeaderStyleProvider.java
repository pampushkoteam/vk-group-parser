package pampushko.vk.parser.cell_styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Провайдер для создания объектов стиля
 * <br>
 * Стиля для ячеек заголовка таблицы
 * <br>
 */
public class HeaderStyleProvider implements Provider<CellStyle>
{
	/**
	 * Книга в которой мы создаём стиль
	 */
	@Inject
	Workbook workbook;
	
	/**
	 * Метод создает стиль для заголовка таблицы
	 *
	 * @return стиль, который затем можно применять к ячейкам таблицы
	 */
	@Override
	public CellStyle get()
	{
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);
		
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);
		
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		return style;
	}
}
