package pampushko.vk.parser;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import pampushko.vk.parser.model.post.Post;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 *
 */
public interface SocialNetworkService
{
	List<Post> getPostsAndCommentsDataOfGroup(String groupScreenName) throws ClientException, ApiException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, NoSuchFieldException, IllegalAccessException, InterruptedException, URISyntaxException;
}
