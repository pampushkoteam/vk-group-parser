var ITERS = 25;
var COUNT = 100;
var posts = [];
var req_params = {
	"owner_id": Args.owner_id,
	"offset": Args.offset,
	"count": Args.count,
	"v": Args.v
};
var i = 0;
while (i < ITERS) {
	req_params.offset = i * COUNT + ITERS * COUNT * Args.offset;
	var items = API.wall.get(req_params).items;

	if (items.length == 0) {
		//noinspection JSAnnotator
		return posts;
	}


	var ids = items
@.
	id;
	var tmp = {};

	tmp.text = items
@.
	text;
	tmp.chunk_size = ids.length;
	tmp.ids = ids;
	tmp.likes = items
@.
	likes
@.
	count;
	tmp.reposts = items
@.
	reposts
@.
	count;
	tmp.commentsNumber = items
@.
	comments
@.
	count;
	tmp.dates = items
@.
	date;
	posts.push(tmp);

	i = i + 1;
}
//noinspection JSAnnotator
return posts;
