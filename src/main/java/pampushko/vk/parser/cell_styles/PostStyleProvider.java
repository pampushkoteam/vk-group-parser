package pampushko.vk.parser.cell_styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Провайдер для создания объектов стиля
 * <br>
 * Стиля для ячеек содержащих данных поста соцсети
 * <br>
 */
public class PostStyleProvider implements Provider<CellStyle>
{
	/**
	 * книга в которой мы создаем стиль
	 */
	@Inject
	Workbook workbook;
	
	/**
	 * метод создает стиль для ячеек с данными постов соцсети
	 *
	 * @return стиль ячеек эпика
	 */
	@Override
	public CellStyle get()
	{
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);
		
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setFontName("Calibri");
		//headerFont.setBold(true);
		
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}
}
