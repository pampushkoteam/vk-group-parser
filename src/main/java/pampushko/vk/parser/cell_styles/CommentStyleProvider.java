package pampushko.vk.parser.cell_styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Провайдер для создания объектов стиля
 * <br>
 * Стиля для ячеек с данными комментариев соцсети
 * <br>
 */
public class CommentStyleProvider implements Provider<CellStyle>
{
	/**
	 * Книга в которой мы создаём стиль
	 */
	@Inject
	Workbook workbook;
	
	/**
	 * метод создает стиль для ячеек excel-таблицы
	 *
	 * @return стиль ячейки
	 */
	@Override
	public CellStyle get()
	{
		CellStyle style = workbook.createCellStyle();
		style.setWrapText(true);
		
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setFontName("Calibri");
		
		style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}
}
