package pampushko.vk.parser.model.comment;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@Builder
public class CommentsResponse
{
	long count;
	CommentOfPost[] items;
}
