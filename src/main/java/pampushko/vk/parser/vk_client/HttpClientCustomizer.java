package pampushko.vk.parser.vk_client;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;

import java.io.IOException;
import java.lang.reflect.Field;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static pampushko.vk.parser.settings.ProxySettingsConstants.*;

/**
 *
 */
public class HttpClientCustomizer
{
	private String proxyHost = MY_PROXY_HOST;
	private Integer proxyPort = MY_PROXY_PORT_INTEGER;
	private int id;
	private String token;
	private int cooldown;
	
	public VkApiClient getVkApiClient() throws NoSuchFieldException, IllegalAccessException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException
	{
		HttpClient httpClient = httpClient();
		TransportClient transportClient = HttpTransportClient.getInstance();
		Field httpClientField = HttpTransportClient.class.getDeclaredField("httpClient");
		httpClientField.setAccessible(true);
		httpClientField.set(null, httpClient);
		
		return new VkApiClient(transportClient);
	}
	
	public HttpClient httpClient() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException
	{
		
		HttpHost targetHost = new HttpHost("localhost", 80, "http");
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
				new AuthScope(targetHost.getHostName(), targetHost.getPort()),
				new UsernamePasswordCredentials("username", "password"));
		
		SSLContextBuilder contextBuilder = new SSLContextBuilder();
		
		contextBuilder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
		
		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(contextBuilder.build());
		
		HttpClientBuilder clientBuilder = HttpClients.custom().setDefaultCredentialsProvider(credsProvider);
		clientBuilder.setSSLSocketFactory(socketFactory);
		RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build();
		clientBuilder.setDefaultRequestConfig(requestConfig);
		
		//		System.setProperty("java.net.socks.username", MY_PROXY_USER_NAME);
		//		System.setProperty("java.net.socks.password", MY_PROXY_PASSWORD);
		//		System.setProperty("socksProxyHost", MY_PROXY_HOST);
		//		System.setProperty("socksProxyPort", MY_PROXY_PORT_STRING);
		//		System.setProperty("socksProxyVersion", "5"); //default 5
		
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
		connectionManager.setMaxTotal(300);
		connectionManager.setDefaultMaxPerRoute(300);
		clientBuilder.setConnectionManager(connectionManager);
		
		BasicCookieStore cookieStore = new BasicCookieStore();
		clientBuilder.setDefaultCookieStore(cookieStore);
		clientBuilder.setUserAgent("Java VK SDK/0.4.3");
		
		clientBuilder.addInterceptorFirst(new HttpRequestInterceptor()
		{
			
			private long lastApiCallTime;
			
			@Override
			public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException
			{
				long cooldown = System.currentTimeMillis() - lastApiCallTime;
				if (cooldown < HttpClientCustomizer.this.cooldown)
				{
					try
					{
						Thread.sleep(HttpClientCustomizer.this.cooldown - cooldown);
					}
					catch (InterruptedException ignored)
					{
					}
				}
				lastApiCallTime = System.currentTimeMillis();
			}
		});
		
		return clientBuilder.build();
	}
}
