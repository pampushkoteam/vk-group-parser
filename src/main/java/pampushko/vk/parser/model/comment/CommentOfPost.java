package pampushko.vk.parser.model.comment;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@Builder
public class CommentOfPost
{
	/**
	 *
	 */
	@SerializedName("id")
	long id;
	
	/**
	 *
	 */
	@SerializedName("from_id")
	long fromId;
	
	/**
	 *
	 */
	@SerializedName("date")
	long date;
	
	/**
	 * Пример: 78702574
	 */
	@SerializedName("reply_to_user")
	long replyToUser;
	
	/**
	 *
	 */
	@SerializedName("reply_to_comment")
	long replyToComment;
	
	/**
	 *
	 */
	@SerializedName("text")
	String text;
	
}
