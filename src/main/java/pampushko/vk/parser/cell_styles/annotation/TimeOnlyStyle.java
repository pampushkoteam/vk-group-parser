package pampushko.vk.parser.cell_styles.annotation;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация обозначающая, что необходимо получить CellStyle для ячеек c датами (в которых необходимо отображать только время, без даты)
 * <br>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@BindingAnnotation
public @interface TimeOnlyStyle
{
}
