package pampushko.vk.parser.excel;

import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 */
public class ExcelSheetSettings
{
	/**
	 *
	 */
	Sheet sheet;
	
	public ExcelSheetSettings(Sheet sheet)
	{
		this.sheet = sheet;
	}
	
	public static void setDefaultRowHeight(Sheet sheet)
	{
		//установили высоту строк (по умолчанию) для таблицы
		//sheet.setDefaultRowHeight((short) 1000);
	}
	
	public static void setColumnsWidth(Sheet sheet)
	{
		
		int columnWidth = 10000;
		sheet.setColumnWidth(0, columnWidth);
		sheet.setColumnWidth(2, 4500);
		sheet.setColumnWidth(4, columnWidth);
		sheet.setColumnWidth(6, 4500);
		sheet.setDefaultColumnWidth(10);
		//		for (int i = 0; i < 10; i++)
		//		{
		//			sheet.setColumnWidth(i, columnWidth);
		//		}
	}
	
	/**
	 * чтобы плюсики в группировке отображались сверху, а не снизу группировки
	 * Это данные - структура - Расположение итоговых данных - итоги в строках под данными - СНЯТЬ ГАЛОЧКУ!
	 *
	 * @param sheet
	 */
	public static void setRowSumsBelow(Sheet sheet)
	{
		//чтобы плюсики в группировке отображались сверху, а не снизу группировки
		//Это данные - структура - Расположение итоговых данных - итоги в строках под данными - СНЯТЬ ГАЛОЧКУ!
		sheet.setRowSumsBelow(false);
	}
	
	/**
	 * метод устанавливает автоматический размер ширины колонок для таблицы
	 * исходя из их содержимого (исходя из того какой размер нужен для корректного отображения содержимого ячейки)
	 *
	 * @param sheet
	 * 		таблица которой мы хотим установить ширину колонок
	 */
	private static void setAutoSizeColumns(Sheet sheet)
	{
		for (int i = 5; i < 20; i++)
		{
			//настраиваем ширину колонок такой чтобы она вычислялась
			//по содержимому самой широкой ячейки в колонке
			sheet.autoSizeColumn(i);
		}
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(3);
		
	}
	
	public static void setGlobalSettingsForSheet(Sheet sheet)
	{
		setColumnsWidth(sheet);
		setDefaultRowHeight(sheet);
		setRowSumsBelow(sheet);
		setAutoSizeColumns(sheet);
		
	}
}
