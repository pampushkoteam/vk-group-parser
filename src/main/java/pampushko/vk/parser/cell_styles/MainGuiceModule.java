package pampushko.vk.parser.cell_styles;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pampushko.vk.parser.cell_styles.annotation.*;
import pampushko.vk.parser.excel.AddCellsOperation;
import pampushko.vk.parser.DocumentGenerator;
import pampushko.vk.parser.excel.ExcelDocumentGeneratorImpl;
import pampushko.vk.parser.excel.ExcelHeaders;
import pampushko.vk.parser.excel.providers.CreationHelperProvider;
import pampushko.vk.parser.excel.providers.DocumentFileWriter;
import pampushko.vk.parser.excel.providers.SheetProvider;
import pampushko.vk.parser.SocialNetworkService;
import pampushko.vk.parser.vk_client.VkInboundAdapter;

/**
 * Модуль настройки DI
 */
public class MainGuiceModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(DocumentGenerator.class).to(ExcelDocumentGeneratorImpl.class).in(Scopes.SINGLETON);
		bind(SocialNetworkService.class).to(VkInboundAdapter.class).in(Scopes.SINGLETON);
		bind(Workbook.class).to(XSSFWorkbook.class).in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(CommentStyle.class)
				.toProvider(CommentStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(DateTimeStyle.class)
				.toProvider(DateTimeStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(DateOnlyStyle.class)
				.toProvider(DateOnlyStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(TimeOnlyStyle.class)
				.toProvider(TimeOnlyStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(HeaderStyle.class)
				.toProvider(HeaderStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(CellStyle.class)
				.annotatedWith(PostStyle.class)
				.toProvider(PostStyleProvider.class)
				.in(Scopes.SINGLETON);
		
		bind(Sheet.class).toProvider(SheetProvider.class).in(Scopes.SINGLETON);
		bind(CreationHelper.class).toProvider(CreationHelperProvider.class).in(Scopes.SINGLETON);
		
		bind(AddCellsOperation.class).in(Scopes.SINGLETON);
		
		bind(DocumentFileWriter.class).in(Scopes.SINGLETON);
		
		bind(ExcelHeaders.class).in(Scopes.SINGLETON);
	}
}

