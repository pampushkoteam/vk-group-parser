package pampushko.vk.parser.model.post;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *  */
@Getter
@Setter
@Builder
public class ResponseDataElement
{
	String[] text;
	long chunk_size;
	long[] ids;
	long[] commentsNumber;
	long[] dates;
	long[] likes;
	long[] reposts;
}
