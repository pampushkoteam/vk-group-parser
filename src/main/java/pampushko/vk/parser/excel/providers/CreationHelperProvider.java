package pampushko.vk.parser.excel.providers;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 *
 */
public class CreationHelperProvider implements Provider<CreationHelper>
{
	@Inject
	Workbook workbook;
	
	@Override
	public CreationHelper get()
	{
		//System.out.println(Math.random());
		return workbook.getCreationHelper();
	}
}
