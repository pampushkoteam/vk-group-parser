package pampushko.vk.parser.excel;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import pampushko.vk.parser.DocumentGenerator;
import pampushko.vk.parser.excel.providers.DocumentFileWriter;
import pampushko.vk.parser.model.comment.CommentOfPost;
import pampushko.vk.parser.model.post.Post;

import javax.inject.Inject;
import java.text.MessageFormat;
import java.util.List;

/**
 * Создаем таблицу excel
 * при этом строки таблицы сгруппированы
 */
public class ExcelDocumentGeneratorImpl implements DocumentGenerator
{
	/**
	 * таблица
	 */
	@Inject
	private Sheet sheet;
	
	/**
	 * вспомогательный класс
	 */
	@Inject
	private CreationHelper creationHelper;
	
	/**
	 * добавление фильтров в excel
	 */
	@Inject
	private ExcelFilters filters;
	
	/**
	 * добавление заголовков в excel
	 */
	@Inject
	private ExcelHeaders headers;
	
	/**
	 * объект для добавления ячеек к строкам таблицы
	 */
	@Inject
	private AddCellsOperation addCellsOperation;
	
	/**
	 * класс для записи документа-результата в файл
	 */
	@Inject
	private DocumentFileWriter documentFileWriter;
	
	/**
	 * печатаем посты и соответствующие им комментарии
	 *
	 * @param postList
	 * 		список постов (у каждого поста есть коллекция комментариев)
	 * @param startRowNumber
	 * 		номер строки с которого мы начнём печатть
	 *
	 * @return номер последней выведенной нами строки
	 */
	private int printPostsAndComments(List<Post> postList, int startRowNumber)
	{
		int rowNumber = startRowNumber;
		for (int i = 0; i < postList.size(); i++)
		{
			Row postRow = sheet.createRow(rowNumber);
			
			Post currentPost = postList.get(i);
			addCellsOperation.addCellsToPostsRow(postRow, postList.get(i));
			
			//увечиваем номер строки
			rowNumber++;
			
			//номер строки таблицы с которого мы будем начинать группировать комментарии
			int rowForStartGrouping = rowNumber;
			
			rowNumber = printComments(currentPost, rowNumber);
			
			int rowForEndGrouping = rowNumber - 1;
			
			if (currentPost.getCommentList().size() > 0)
			{
				//группируем задачи
				sheet.groupRow(rowForStartGrouping, rowForEndGrouping);
			}
		}
		return sheet.getLastRowNum();
	}
	
	/**
	 * печатаем список комментариев поста
	 *
	 * @param post
	 * 		объект поста содержащий коллекцию комментариев
	 * @param startRowNumber
	 * 		начальное значение строки таблицы, мы начинаем печатать данные с позиции startRowNumber
	 *
	 * @return номер последней выведенной нами строки
	 */
	private int printComments(Post post, int startRowNumber)
	{
		int rowNumber = startRowNumber;
		//внутри поста создаем комментарии
		List<CommentOfPost> commentList = post.getCommentList();
		int commentsListSize = commentList.size();
		int commentsCounter = 0;
		
		while (commentsCounter < commentsListSize)
		{
			//создаем строку комментария
			Row commentRow = sheet.createRow(rowNumber);
			addCellsOperation.addCellsToCommentsRow(commentRow, commentList.get(commentsCounter));
			rowNumber++;
			commentsCounter++;
		}
		return rowNumber;
	}
	
	/**
	 * метод формирует эксель таблицу с группировками строк и заданным набором колонок
	 * значения ячеек таблицы заполнены номерами строк в которых расположены эти ячейки
	 *
	 * @param postList
	 * 		список постов на основании которых мы будем генерировать таблицу
	 */
	@Override
	public void createDocumentFromPostList(List<Post> postList, String documentName)
	{
		ExcelSheetSettings.setGlobalSettingsForSheet(sheet);
		
		//печатаем заголовок нашей таблицы
		//нумерация строк в таблице начинается с нуля
		int startRowNumber = 0;
		int lastRowNumber = headers.printMainHeader(startRowNumber);
		
		//следующие строки печатаем со большей на единицу позиции
		startRowNumber = lastRowNumber++;
		
		lastRowNumber = printPostsAndComments(postList, 1);
		
		System.out.println(MessageFormat.format("Всего было выведено {0} строк ", lastRowNumber - startRowNumber));
		
		String filename = documentFileWriter.writeFileToDisk(documentName);
		System.out.println(filename);
	}
}
