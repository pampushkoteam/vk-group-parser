package pampushko.vk.parser.model.post;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pampushko.vk.parser.model.comment.CommentOfPost;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Getter
@Setter
@Builder
@ToString(includeFieldNames = true)
public class Post
{
	@SerializedName("text")
	String text;
	@SerializedName("ids")
	long ids;
	@SerializedName("commentsNumber")
	long commentsNumber;
	@SerializedName("dates")
	long dates;
	List<CommentOfPost> commentList;
	
	public List<CommentOfPost> getCommentList()
	{
		if (commentList == null)
		{
			commentList = new ArrayList<>();
		}
		return commentList;
	}
}
