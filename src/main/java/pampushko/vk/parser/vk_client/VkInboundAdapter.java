package pampushko.vk.parser.vk_client;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.utils.DomainResolved;
import com.vk.api.sdk.objects.utils.DomainResolvedType;
import pampushko.vk.parser.SocialNetworkService;
import pampushko.vk.parser.model.comment.CommentOfPost;
import pampushko.vk.parser.model.comment.CommentsResponse;
import pampushko.vk.parser.model.post.Post;
import pampushko.vk.parser.model.post.ResponseDataElement;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class VkInboundAdapter implements SocialNetworkService
{
	@Override
	public List<Post> getPostsAndCommentsDataOfGroup(String groupScreenName) throws ClientException, ApiException,
			IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, NoSuchFieldException,
			IllegalAccessException, InterruptedException, URISyntaxException
	{
		HttpClientCustomizer customizer = new HttpClientCustomizer();
		VkApiClient vk = customizer.getVkApiClient();
		
		String token = "18a68b450b9d80dcc88a467b7b3fd366995fe2a46cf71454f0abc242b40a4ea482b18839122e40661b8be";
		int userId = 482628726;
		UserActor actor = new UserActor(userId, token);
		System.out.println(actor.toString());
		
		URL urlPosts = Resources.getResource("vkScript_getPost.js");
		System.out.println(urlPosts);
		String scriptPosts = Resources.toString(urlPosts, Charsets.UTF_8);
		
		URL urlComments = Resources.getResource("vkScript_getComments.js");
		System.out.println(urlComments);
		String scriptComments = Resources.toString(urlComments, Charsets.UTF_8);
		
		DomainResolved result = vk.utils().resolveScreenName(actor, groupScreenName).execute();
		
		int groupOrUserId = result.getObjectId();
		
		DomainResolvedType domainResolvedType = result.getType();
		boolean isGroup = false;
		if (result.getType().equals(DomainResolvedType.GROUP))
		{
			groupOrUserId = groupOrUserId * -1;
		}
		System.out.println(MessageFormat.format("Group id = {0}", groupOrUserId));
		
		Thread.sleep(500);
		
		JsonElement responsePosts = vk.execute().code(actor, scriptPosts)
				.unsafeParam("owner_id", groupOrUserId)
				.unsafeParam("offset", "0")
				.unsafeParam("count", "100")
				.unsafeParam("v", "5.73")
				.execute();
		
		//список всех постов группы
		List<Post> postsResultList = new ArrayList<>();
		
		Gson gson = new Gson();
		Type listType = new TypeToken<List<ResponseDataElement>>()
		{
		}.getType();
		List<ResponseDataElement> posts = new Gson().fromJson(responsePosts, listType);
		
		for (ResponseDataElement postElement : posts)
		{
			String[] text = postElement.getText();
			long chunk_size = postElement.getChunk_size();
			long[] ids = postElement.getIds();
			long[] commentsNumber = postElement.getCommentsNumber();
			long[] dates = postElement.getDates();
			for (int i = 0; i < chunk_size; i++)
			{
				postsResultList.add(Post.builder().ids(ids[i]).text(text[i]).dates(dates[i]).commentsNumber(commentsNumber[i]).build());
			}
		}
		
		for (int i = 0; i < postsResultList.size(); i++)
		//for(int i = 0; i < 3; i++)
		{
			Thread.sleep(500);
			Post currentPost = postsResultList.get(i);
			JsonElement responseComments = vk.execute().code(actor, scriptComments)
					.unsafeParam("owner_id", groupOrUserId)
					.unsafeParam("post_id", currentPost.getIds())
					.unsafeParam("offset", "0")
					.unsafeParam("count", "3000")
					.unsafeParam("v", "5.73")
					.execute();
			System.out.println(responseComments);
			
			CommentsResponse commentsResponse = new Gson().fromJson(responseComments, CommentsResponse.class);
			List<CommentOfPost> commentList = new ArrayList<CommentOfPost>(Arrays.asList(commentsResponse.getItems()));
			currentPost.setCommentList(commentList);
		}
		return postsResultList;
	}
}
