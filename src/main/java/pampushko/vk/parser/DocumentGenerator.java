package pampushko.vk.parser;

import pampushko.vk.parser.model.post.Post;

import java.util.List;

/**
 * генератор excel документа из списка постов и вместе с комментариями к постам
 */
public interface DocumentGenerator
{
	void createDocumentFromPostList(List<Post> postList, String documentName);
}
