package pampushko.vk.parser.excel.providers;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 *
 */
public class SheetProvider implements Provider<Sheet>
{
	@Inject
	Workbook workbook;
	
	@Override
	public Sheet get()
	{
		return workbook.createSheet();
	}
}
