package pampushko.vk.parser;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class MainController
{
	@FXML
	TextField groupUrlTextField;
	@FXML
	TextField groupNameTextField;
	@FXML
	TableView tableView;
	
	public void quit(ActionEvent actionEvent)
	{
	
	}
	
	public void about(ActionEvent actionEvent)
	{
	
	}
	
	public void searchTextfieldKeyReleased(KeyEvent keyEvent)
	{
	
	}
	
	public void addButton(ActionEvent actionEvent)
	{
		
		if (groupNameTextField.getText().trim().length() > 0)
		{
			Main.data.add(new VkGroup(
					groupNameTextField.getText().trim(),
					groupUrlTextField.getText().trim()));
			groupNameTextField.clear();
			groupUrlTextField.clear();
		}
	}
	
	public void getCurrentGroupData(ActionEvent actionEvent)
	{
		final List<VkGroup> selected = new ArrayList<>(tableView.getSelectionModel().getSelectedItems());
		processData(selected);
	}
	
	public void getAllGroupsData(ActionEvent actionEvent)
	{
		final List<VkGroup> allItems = new ArrayList<>(tableView.getItems());
		processData(allItems);
	}
	
	public void processData(List<VkGroup> selected)
	{
		List<String> groupList = new ArrayList<>();
		selected.forEach(x -> groupList.add(x.getFirstName()));
		try
		{
			Main.run(groupList);
		}
		catch (ApiException | NoSuchAlgorithmException | NoSuchFieldException | InterruptedException
				| IllegalAccessException | ClientException | IOException | KeyStoreException | KeyManagementException |
				URISyntaxException ex)
		{
			ex.printStackTrace();
		}
	}
}
