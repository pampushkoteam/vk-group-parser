package pampushko.vk.parser.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import pampushko.vk.parser.cell_styles.annotation.HeaderStyle;

import javax.inject.Inject;

/**
 *
 */
public class ExcelHeaders
{
	@Inject
	private Sheet sheet;
	
	@Inject
	@HeaderStyle
	private CellStyle cellStyle;
	
	//набор строк которые станут названия колонок в формируемой нами таблице
	public static final String[] COLUMN_HEADERS =
			{
					"Текст поста",
					"id поста",
					"Дата",
					"Кол-во комментариев",
					"Комментарий.",
					"id коментария",
					"Дата + Время",
					"Дата",
					"Время",
					"id автора",
					"Ответ на комент с id",
					"Ответ пользователю c id"
				
			};
	
	public static void main(String[] args)
	{
		System.out.println(COLUMN_HEADERS.length);
	}
	
	/**
	 * Метод печатает заловок таблицы
	 *
	 * @param startRowNumber
	 * 		номер строки в которой мы этот заголовок печатаем (обычно 0, но может быть и другой номер если мы хотим может распечатать заголовок в конце или внутри таблицы)
	 *
	 * @return возращаем номер последней строки таблицы (на момент когда были выведены все строки заголовка)
	 */
	public int printMainHeader(int startRowNumber)
	{
		Row header_row = sheet.createRow(startRowNumber);
		header_row.setHeight((short) 1500);
		
		for (int i = 0; i < COLUMN_HEADERS.length; i++)
		{
			Cell cell = header_row.createCell(i);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(COLUMN_HEADERS[i]);
		}
		return sheet.getLastRowNum();
	}
}
