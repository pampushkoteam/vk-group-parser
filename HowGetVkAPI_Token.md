# Как получить токен для работы с API Vkontakte?
Для этого необходимо в браузере открыть URL вида:


https://oauth.vk.com/authorize?client_id=5490057&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=notify,friends,photos,audio,video,stories,pages,status,notes,messages,wall,ads,offline,docs,groups,notifications,stats,email,market%20&response_type=token&v=5.73

* Параметр &scope определяет перечень разрешений, которые будут предоставлять клиенту,
отправляющему запрос с полученным токеном

* Укажите в ссылке свой client_id

Если вы не авторизованы в Vk, то вам предложат авторизоваться, а затем подтвердить,
что вы хотите получить токен.

Дополнительную информацию читайте по ссылке: https://vk.com/dev/implicit_flow_user

---
После этого вы увидите в адресной строке браузера такой URL :
https://oauth.vk.com/blank.html#access_token=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&expires_in=0&user_id=123456789

Параметр access_token - это сгенерированный по нашему запросу токен.

А expires_in=0 значит, что токен будет действовать бессрочно, и не перестанет работать через некоторое время.

user_id - показывает для какого пользователя этот токен был выпущен.

