package pampushko.vk.parser;

import com.google.common.io.Resources;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pampushko.vk.parser.cell_styles.MainGuiceModule;
import pampushko.vk.parser.excel.ExcelDocumentGeneratorImpl;
import pampushko.vk.parser.model.post.Post;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Main extends Application
{
	public Scene scene = null;
	
	public static void run(List<String> groupList) throws ApiException, NoSuchAlgorithmException, NoSuchFieldException,
			InterruptedException, IllegalAccessException, ClientException, IOException, KeyStoreException, KeyManagementException, URISyntaxException
	{
		Injector injector = Guice.createInjector(new MainGuiceModule());
		ExcelDocumentGeneratorImpl excelGenerator = injector.getInstance(ExcelDocumentGeneratorImpl.class);
		
		SocialNetworkService socialNetworkService = injector.getInstance(SocialNetworkService.class);
		DocumentGenerator documentGenerator = injector.getInstance(DocumentGenerator.class);
		
		for (String element : groupList)
		{
			List<Post> posts = socialNetworkService.getPostsAndCommentsDataOfGroup(element);
			documentGenerator.createDocumentFromPostList(posts, element);
		}
	}
	
	public static final ObservableList<VkGroup> data =
			FXCollections.observableArrayList(
					new VkGroup("id329428998", "id329428998"),
					new VkGroup("tipkhimki", "tipkhimki"),
					new VkGroup("voloshin_d_v", "voloshin_d_v")
			);
	
	@Override
	public void start(Stage stage) throws Exception
	{
		URL resource1 = Resources.getResource("main.fxml");
		scene = new Scene(
				FXMLLoader.<Parent>load(resource1),
				1024, 600);
		
		TableView table = (TableView) scene.lookup("#menu_data_tableview");
		table.setEditable(true);
		TableColumn<VkGroup, String> firstNameCol = new TableColumn<>("Имя группы");
		firstNameCol.setMinWidth(100);
		firstNameCol.setCellValueFactory(
				new PropertyValueFactory<VkGroup, String>("firstName"));
		
		TableColumn<VkGroup, String> lastNameCol = new TableColumn<>("URL группы");
		lastNameCol.setMinWidth(100);
		lastNameCol.setCellValueFactory(
				new PropertyValueFactory<VkGroup, String>("lastName"));
		table.setItems(data);
		table.getColumns().addAll(Arrays.asList(firstNameCol, lastNameCol));
		
		final ContextMenu tableContextMenu = new ContextMenu();
		//		final MenuItem addMenuItem = new MenuItem("Add...");
		final MenuItem deleteSelectedMenuItem = new MenuItem("Delete selected");
		deleteSelectedMenuItem.disableProperty().bind(Bindings.isEmpty(table.getSelectionModel().getSelectedItems()));
		deleteSelectedMenuItem.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
			{
				final List<VkGroup> selectedPeople = new ArrayList<>(table.getSelectionModel().getSelectedItems());
				table.getItems().removeAll(selectedPeople);
			}
		});
		tableContextMenu.getItems().addAll(deleteSelectedMenuItem);
		table.setContextMenu(tableContextMenu);
		
		stage.setScene(scene);
		stage.show();
	}
}
