package pampushko.vk.parser;

import javafx.fxml.Initializable;
import javafx.scene.control.ScrollBar;

import javax.swing.text.html.parser.ContentModel;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller class for settings panel
 */
public class NavigationController implements Initializable
{
	//    public FourWayNavControl eyeNav;
	public ScrollBar zoomBar;
	//    public FourWayNavControl camNav;
	private ContentModel contentModel;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		zoomBar.setMin(-100);
		zoomBar.setMax(0);
		
		zoomBar.setVisibleAmount(5);
		
		//        eyeNav.setListener(new FourWayNavControl.FourWayListener() {
		//            @Override public void navigateStep(Side direction, double amount) {
		//                switch (direction) {
		//                    case TOP:
		//                        contentModel.getCameraLookXRotate().setAngle(contentModel.getCameraLookXRotate().getAngle()+amount);
		//                        break;
		//                    case BOTTOM:
		//                        contentModel.getCameraLookXRotate().setAngle(contentModel.getCameraLookXRotate().getAngle()-amount);
		//                        break;
		//                    case LEFT:
		//                        contentModel.getCameraLookZRotate().setAngle(contentModel.getCameraLookZRotate().getAngle()-amount);
		//                        break;
		//                    case RIGHT:
		//                        contentModel.getCameraLookZRotate().setAngle(contentModel.getCameraLookZRotate().getAngle()+amount);
		//                        break;
		//                }
		//            }
		//        });
		//        camNav.setListener(new FourWayNavControl.FourWayListener() {
		//            @Override public void navigateStep(Side direction, double amount) {
		//                switch (direction) {
		//                    case TOP:
		//                        contentModel.getCameraXRotate().setAngle(contentModel.getCameraXRotate().getAngle()-amount);
		//                        break;
		//                    case BOTTOM:
		//                        contentModel.getCameraXRotate().setAngle(contentModel.getCameraXRotate().getAngle()+amount);
		//                        break;
		//                    case LEFT:
		//                        contentModel.getCameraYRotate().setAngle(contentModel.getCameraYRotate().getAngle()+amount);
		//                        break;
		//                    case RIGHT:
		//                        contentModel.getCameraYRotate().setAngle(contentModel.getCameraYRotate().getAngle()-amount);
		//                        break;
		//                }
		//            }
		//        });
	}
}
