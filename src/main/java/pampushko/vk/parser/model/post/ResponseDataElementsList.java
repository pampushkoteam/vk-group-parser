package pampushko.vk.parser.model.post;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@Builder
public class ResponseDataElementsList
{
	ResponseDataElement[] elements;
}
