package pampushko.vk.parser.excel.providers;

import com.google.common.io.Files;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */
public class DocumentFileWriter
{
	private Workbook workbook;
	
	@Inject
	public DocumentFileWriter(Workbook workbook)
	{
		this.workbook = workbook;
	}
	
	public String writeFileToDisk(String documentName)
	{
		String fileName = "";
		try
		{
			File tempDir = Files.createTempDir();
			//fileName = tempDir + File.separator + documentName + ".xlsx";
			fileName = documentName + ".xlsx";
			FileOutputStream fileOutputStream = new FileOutputStream(fileName);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
		}
		catch (FileNotFoundException ex)
		{
			ex.printStackTrace(System.err);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
		return fileName;
	}
	
}
