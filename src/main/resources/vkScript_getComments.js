var post_id = Args.post_id;
var owner_id = Args.owner_id;
var count = Args.count;
var offset = Args.offset;

if (post_id == null || owner_id == null) {
	//noinspection JSAnnotator
	return {"count": 0, "items": []};  // Если параметры не заданы, возвращаем пустой массив
}
if (count == null) {
	count = 10; // Значение по умолчанию
}
if (offset == null) {
	offset = 0; // Значение по умолчанию
}
var i = 0;
var items = [];
while (i < 25 && count > items.length) {
	var cur_count = count - items.length;
	if (cur_count > 100) {
		cur_count = 100;
	}
	items = items + API.wall.getComments({
			"owner_id": owner_id,
			"post_id": post_id,
			"offset": offset + 100 * i,
			"count": cur_count
		})["items"];
	i = i + 1;
}
//noinspection JSAnnotator
return {"count": items.length, "items": items};
