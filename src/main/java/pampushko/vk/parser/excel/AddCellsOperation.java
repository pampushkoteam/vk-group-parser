package pampushko.vk.parser.excel;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import pampushko.vk.parser.cell_styles.annotation.*;
import pampushko.vk.parser.model.comment.CommentOfPost;
import pampushko.vk.parser.model.post.Post;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

/**
 * класс содержит операции по добавлению ячеек различного типа к строкам таблицы
 */
public class AddCellsOperation
{
	/**
	 * стиль для ячеек поста
	 */
	@Inject
	@PostStyle
	CellStyle postCellStyle;
	
	/**
	 * стиль для ячеек комментария к посту
	 */
	@Inject
	@CommentStyle
	CellStyle commentCellStyle;
	
	/**
	 * стиль для ячеек с датой+временем
	 */
	@Inject
	@DateTimeStyle
	CellStyle dateTimeCellStyle;
	
	/**
	 * стиль для ячеек с датой-временем, которые отображают только дату
	 */
	@Inject
	@DateOnlyStyle
	CellStyle dateOnlyCellStyle;
	
	/**
	 * стиль для ячеек с датой-временем, которые отображают только время
	 */
	@Inject
	@TimeOnlyStyle
	CellStyle timeOnlyCellStyle;
	
	/**
	 * @param row
	 * 		строка к которой мы хотим добавить ячейку
	 * @param column
	 * 		позиция создаваемой ячейки в строке
	 * @param data
	 * 		данные, которые будут помещены в создаваемую ячейку
	 * @param style
	 * 		стиль, который будет применён к создаемой ячейке
	 */
	public void addStringCell(Row row, int column, String data, CellStyle style)
	{
		Cell cell = row.createCell(column);
		cell.setCellStyle(style);
		cell.setCellValue(data);
	}
	
	/**
	 * @param row
	 * 		строка к которой мы хотим добавить ячейку
	 * @param column
	 * 		позиция создаваемой ячейки в строке
	 * @param data
	 * 		данные, которые будут помещены в создаваемую ячейку
	 * @param style
	 * 		стиль, который будет применён к создаемой ячейке
	 */
	public void addLongCell(Row row, int column, long data, CellStyle style)
	{
		Cell cell = row.createCell(column);
		cell.setCellStyle(style);
		cell.setCellValue(data);
	}
	
	/**
	 * @param row
	 * 		строка к которой мы хотим добавить ячейку
	 * @param column
	 * 		позиция создаваемой ячейки в строке
	 * @param epochTime
	 * @param style
	 * 		стиль, который будет применён к создаемой ячейке
	 */
	public void addDateTimeCellFromEpochTime(Row row, int column, long epochTime, CellStyle style)
	{
		Cell cell = row.createCell(column);
		cell.setCellStyle(style);
		cell.setCellValue(new Date(epochTime * 1000)); //todo vk содержит отметки времени в секунда, а не миллисекундах
	}
	
	/**
	 * @param row
	 * 		строка к которой мы хотим добавить ячейку
	 * @param column
	 * 		позиция создаваемой ячейки в строке
	 * @param epochTime
	 * @param style
	 * 		стиль, который будет применён к создаемой ячейке
	 */
	public void addDateOnlyCellFromEpochTime(Row row, int column, long epochTime, CellStyle style)
	{
		Cell cell = row.createCell(column);
		cell.setCellStyle(style);
		cell.setCellValue(DateUtils.truncate(new Date(epochTime * 1000), Calendar.DATE)); //todo vk содержит отметки времени в секунда, а не миллисекундах
	}
	
	/**
	 * @param row
	 * 		строка к которой мы хотим добавить ячейку
	 * @param column
	 * 		позиция создаваемой ячейки в строке
	 * @param epochTime
	 * @param style
	 * 		стиль, который будет применён к создаемой ячейке
	 */
	public void addTimeOnlyCellFromEpochTime(Row row, int column, long epochTime, CellStyle style)
	{
		Cell cell = row.createCell(column);
		cell.setCellStyle(style);
		cell.setCellValue(new Date(epochTime * 1000)); //todo vk содержит отметки времени в секунда, а не миллисекундах
	}
	
	/**
	 * Формируем из ячеек и данных содержащихся в объекте post, строку таблицы с данным поста
	 *
	 * @param row
	 * 		строка в которую мы хотим добавить ячеек
	 * @param post
	 * 		данные для заполнения ячеек
	 */
	public void addCellsToPostsRow(Row row, Post post)
	{
		addStringCell(row, 0, post.getText(), postCellStyle);
		addLongCell(row, 1, post.getIds(), postCellStyle);
		addDateTimeCellFromEpochTime(row, 2, post.getDates(), dateTimeCellStyle);
		addLongCell(row, 3, post.getCommentsNumber(), postCellStyle);
	}
	
	/**
	 * Формируем из ячеек и данных содержащихся в объекте comment, строку таблицы с данным комментария
	 *
	 * @param row
	 * 		строка в которую мы хотим добавить ячеек
	 * @param comment
	 * 		данные для заполнения ячеек
	 */
	public void addCellsToCommentsRow(Row row, CommentOfPost comment)
	{
		addStringCell(row, 4, comment.getText(), commentCellStyle);
		addLongCell(row, 5, comment.getId(), commentCellStyle);
		
		long commentEpochTime = comment.getDate();
		addDateTimeCellFromEpochTime(row, 6, commentEpochTime, dateTimeCellStyle);
		addDateOnlyCellFromEpochTime(row, 7, commentEpochTime, dateOnlyCellStyle);
		addTimeOnlyCellFromEpochTime(row, 8, commentEpochTime, timeOnlyCellStyle);
		
		addLongCell(row, 9, comment.getFromId(), commentCellStyle);
		addLongCell(row, 10, comment.getReplyToComment(), commentCellStyle);
		addLongCell(row, 11, comment.getReplyToUser(), commentCellStyle);
	}
}
