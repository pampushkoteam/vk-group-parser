package pampushko.vk.parser.excel;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.inject.Inject;

import static pampushko.vk.parser.excel.ExcelHeaders.COLUMN_HEADERS;

/**
 *
 */
public class ExcelFilters
{
	@Inject
	Sheet sheet;
	
	public void addFilters()
	{
		int firstCol = 0;
		int lastCol = COLUMN_HEADERS.length - 1;
		int firstRow = sheet.getFirstRowNum();
		int lastRow = sheet.getLastRowNum();
		
		sheet.setAutoFilter(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
	}
}
