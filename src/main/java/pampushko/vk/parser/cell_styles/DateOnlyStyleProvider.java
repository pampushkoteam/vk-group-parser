package pampushko.vk.parser.cell_styles;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Провайдер для создания объектов стиля
 * <br>
 * Стиля для ячеек с данными содержащими дату-время с отображением только даты
 * <br>
 */
public class DateOnlyStyleProvider implements Provider<CellStyle>
{
	public static final String DATE_FORMAT = "DD.MM.YYYY";
	
	/**
	 * Книга в которой мы создаём стиль
	 */
	@Inject
	Workbook workbook;
	
	@Inject
	CreationHelper creationHelper;
	
	/**
	 * метод создает стиль для ячеек excel-таблицы
	 *
	 * @return стиль ячейки
	 */
	@Override
	public CellStyle get()
	{
		CellStyle cellStyle = workbook.createCellStyle();
		short dataFormat = creationHelper.createDataFormat().getFormat(DATE_FORMAT);
		cellStyle.setDataFormat(dataFormat);
		return cellStyle;
	}
}
